// variables
// type > 16 is a data block ( min size 16)
// int,double or block
// strings are in blocks
// have a list and data groups


// as soon as a data item is declared we add access functions to the main socket
//
// actions before read and after set
// groups have actions as well.

#ifndef __VARS_H
#define __VARS_H

#include <string>
#include "array.h"

using namespace std;



struct Var {

  Var();
  ~Var();
  Var(char *vname, char *vdesc, int vtype);

public:
  int id;
  int vartype;
  string name;
  string desc;
private:
  int valint;
  double valdouble;
  void *valdata;
  Array *varGroupArray;
};

struct VarGroup {
  VarGroup(){};
  ~VarGroup(){};
public:
  int group_id;
  string name;
  string desc;
  Array *varGroupArray;
};

struct VarType {
  VarType (string iname, string idesc, int ivcode) {
    name = iname;
    desc=idesc;
    vcode= ivcode;
  };
  VarType(){};

public:
  string name;
  string desc;
  int vcode;
};


#endif

