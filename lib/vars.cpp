// variables
// change of plan .. moved across to varitems




// type > 16 is a data block ( min size 16)
// int,double or block
// strings are in blocks
// have a list and data groups


// as soon as a data item is declared we add access functions to the main socket
//
// actions before read and after set
// groups have actions as well.


//varitems have varitems etc


#include <iostream>
#include <string.h>
#include <string>

#include "array.h"
#include "vars.h"
#include "tests.h"

using namespace std;

int g_varId = 0;
int g_varGroupId = 0;

Array *g_varArray = NULL;
Array *g_varIdArray = NULL;
Array *g_varGArray = NULL;
Array *g_vtArray = NULL;


Var::Var(){
  id = g_varId++;
  if(g_varIdArray)g_varIdArray->addId(id, this);
}

Var::~Var(){
  if(g_varArray)g_varArray->_delItem((void *) this);
  if(g_varIdArray)g_varIdArray->delId(id,this);
  int idx = 0;
  struct VarGroup*gvar=NULL;
  while (varGroupArray->findNext((void **) &gvar, idx, 0)) {
    gvar->varGroupArray->_delItem(this);
    gvar =  NULL;
  }
  if(valdata) free(valdata);
}

Var::Var(char *vname, char *vdesc, int vtype) {
  id = g_varId++;
  name = (string)vname;
  desc = (string)vdesc;
  vartype = vtype;
  valint = 0;
  valdouble = 0.0;
  if(vtype > 4) 
    valdata = malloc(vtype);
  else
    valdata = NULL;
  if(g_varIdArray)g_varIdArray->addId(id, this);
  if(g_varArray)g_varArray->addItem(this);
  
}


int addType(string name, string desc, int vcode)
{
  VarType * vt = new VarType(name, desc, vcode);
  if(g_vtArray == NULL )
    g_vtArray = new Array(16,16,"Vartypes");
  g_vtArray->addId(vcode, vt);
  return 0;
}

int initVars(void)
{
  addType("Digin",     "Digital Input",   1);
  addType("Digout",    "Digital Output",  2);
  addType("Analogin",  "Analog Input",    3);
  addType("Analogout", "Analog Output",   4);
  addType("Servo",     "Servo Output",    5);
  addType("PWD",       "PWM Output",      6);
  addType("Group",     "Group Of Things", 10);
  addType("List",      "List Of Things",  20);
  addType("Func",      "Function",        100);
  return 0;
}

#ifdef _MAIN


int showVars(void *tdest, char *cmd, void *data)
{
  int idx=0;
  Var *var =  NULL;
  while( g_varArray->findNext((void **)&var, idx , 0)) {
    cout << " Var (" << var->id<<" name [" << var->name << "] desc ["<< var->desc<<" ]" << endl;
  }
  return 0;
}

int addVars(void *tdest, char *cmd, void *data)
{
  int idx=0;
  Var *var =  NULL;
  char c[128];
  char n[128];
  char d[128];
  int t;
  int rc;
  rc = sscanf(cmd,"%127s %127s %127s %d"
	       ,c,n,d,&t);
  if (rc > 3) {
    var = new Var( n ,d ,t);
  } else {
    cout << " addVar [name] [desc] type (rc "<<rc<<") "<< endl;
  }
  return 0;
}


int varTypes(void *tdest, char *cmd, void *data)
{
  int t;
  int rc;
  VarType * vt = NULL;
  int idx = 0;
  if(g_vtArray) {
    while (g_vtArray->findNext((void **) &vt, idx, 0)) {
      cout << " name " << vt->name
	   << " desc " << vt->desc
	   << " code" << vt->vcode<< endl;
    }
  }
  return 0;
}



int main(int argc, char *argv[])
{

  g_varArray=new Array(16,16, "Vars");
  g_varIdArray=new Array(16,16, "Var Ids");
  g_varGArray=new Array(16,16, "Var Groups");
  initVars();
  Test *t = new Test("vars", "Show Vars", showVars, NULL);
  t = new Test("addVar", "Add Var", addVars, NULL);
  t = new Test("varTypes", "ShowVar Types", varTypes, NULL);


  for (int i=1 ; i < argc ; i++ ) {
    runTest(NULL, argv[i]);
  }

  // close down
  
  Var *var = NULL;
  int idx = 0;
  while(g_varArray->findNext((void **) &var, idx, 0)) {
    delete var;
    var = NULL;
    
  }

  VarGroup *vg = NULL;
  idx = 0;
  while(g_varGArray->findNext((void **) &vg, idx, 0)) {
    delete vg;
    vg = NULL;
    
  }
  
  delete g_varArray; 
  delete g_varIdArray; 
  delete g_varGArray; 

  return 0;
}


#endif
