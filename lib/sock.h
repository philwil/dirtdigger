/**
 * file sock.h
 * Super Sock Class.
 *
 * g++ -o sock -D_MAIN sock.cpp -lpthread -lrt
 */
#ifndef ___SSOCK_H
#define ___SSOCK_H

#include <malloc.h>
#include <pthread.h>
#include <syslog.h>
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "array.h"

 
using namespace std;


struct sock_command;
typedef struct sock_command sock_command_t;
struct SockThread;
typedef struct SockThread SockThread_t; 

#define DEF_MAX_ID 65535
#define SOCK_SERVER 1
#define SOCK_CLIENT 2


struct SockCommand {
  SockCommand(string ckey, string chelp, int (*caction)(struct SockThread* st, char *msg, void *data), void *data);

  ~SockCommand(){};
  string key;
  string help;
  void * data;
  int (*action)(struct SockThread* st, char *msg, void *data);
};

  
struct Sock {
  // default
  Sock(){sockfd=-1;port=0;};

  Sock(string xaddr, int xport);
  
  Sock(int xport);
  
  ~Sock() {
    //if(addr) free(addr);
    if(sockfd > 0) close(sockfd);
  };

  int saccept(void);
  
public:
  int runCmd(SockThread *st, char *msg);
  
  int addCmd( string key, string help, int (*action)(struct SockThread* st, char *msg, void *data), void *data);
  int runAccept(Array *srvArray);

  sockaddr_in serAdd;
  sockaddr_in cliAdd;
  int type;
  int port;
  string addr;
  int sockfd;
  Array *stArray;
  Array *cmdArray;
  int skdone;
  pthread_t st;

  // struct addrinfo ainfo;
  //struct addrinfo *ares; = NULL;
  //struct addrinfo *asave; = NULL;
};


void *sockThread(void *data);

struct SockThread {
  SockThread(Sock *s, int fd, void *data) {
    sk = s;
    sfd = fd;
    sdata = data;
    buf_len = sizeof(buf);
    msgnum = 0;
    pthread_create(&pt, NULL, sockThread, (void *) this);
  };
  ~SockThread(){};

  int msgnum;
  char buf[1024];
  int buf_len;
  pthread_t pt;
  void *sdata;
  int sfd;
  Sock *sk;
};



struct sock_command
{
  char *key;
  char *help;
  int (*action)(SockThread_t *sthread, char *msg, void *data);
  //  int (*old_action)(netc_thread_t *net_th, char *msg);
  int port;
  int active;
  void *data;

  //struct list_head clist;

  sock_command_t *next;

};


int cmd_help(struct SockThread*st, char *msg, void *data);
int cmd_quit(struct SockThread*st, char *msg, void *data);
int cmd_socks(struct SockThread*st, char *msg, void *data);

#endif
