//// servo class driver

#ifndef __SERVO_H
#define __SERVO_H

#include "sock.h"
#include "array.h"
#include "tests.h"


using namespace std;

struct Servo {
  Servo(int pin);
  Servo(char *sname, char *spin, char *spos);
  ~Servo();

  void setVal(int val, int time);
  void setVal(char *spos, char * stime);

  string name;
  int pin;

  int pos;
  int rpos;
  int time;
  int incr;
  int pc;


  int on;
  int off;
  int count;

};





#endif
