// basic servo movement


#include <iostream>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>

#include "array.h"
#include "tests.h"
#include "servo.h"

using namespace std;
Array *g_servoArray = NULL;

pthread_t runServopth;
int g_runServoDone = 0;

#define SERVO_RATE_MS  10


int SetServoOutput(int pin, int pos, int count)
{
  cout << "servo pin " << pin<<" new pos " << pos << "count " << count << endl;
}

int sleepmS(struct timespec *now, int ms)
{
  if (ms < 0) {
      clock_gettime(CLOCK_MONOTONIC, now);
      return 0;
    }
    // Add the time you want to sleep
  now->tv_nsec += (1000000 * ms);
  
  // Normalize the time to account for the second boundary
  if(now->tv_nsec >= 1000000000) {
      now->tv_nsec -= 1000000000;
      now->tv_sec++;
    }
  clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, now, NULL);
  return 0;
  
}

int setServoVal(string name, char *spos, char *stime)
{
  int idx= 0;
  Servo *servo= NULL;
  if(g_servoArray) {
    while (g_servoArray->findNext((void**)&servo, idx, 0) ) {
      if(servo->name == name ) {
	
	servo->setVal(spos, stime);
	return 0;
      }
    }
  }
  return 0;
} 



int showServoVal(string name)
{
  int idx= 0;
  Servo *servo= NULL;
  if(g_servoArray) {
    while (g_servoArray->findNext((void**)&servo, idx, 0) ) {
      if(servo->name == name ) {
	
	servo->pc+=1;
	return 0;
      }
    }
  }
  return 0;
} 

void *runServo(void *data)
{
  struct timespec now;

  int idx;
  Servo *servo;

  sleepmS(&now, -1);
  
  while (!g_runServoDone) {
    sleepmS(&now, SERVO_RATE_MS);
    

    idx = 0;
    servo = NULL;
    if(g_servoArray) {
      while (g_servoArray->findNext((void**)&servo, idx, 0) ) {
	if(servo->pos != servo->rpos) {
	  if(servo->pos > servo->rpos) {
	    servo->pos -= servo->incr;
	    if(servo->pos < servo->rpos) {
	      servo->pos = servo->rpos;
	      servo->time = 0;
	    }
	  }
	  if(servo->pos < servo->rpos) {
	    servo->pos += servo->incr;
	    if(servo->pos > servo->rpos) {
	      servo->pos = servo->rpos;
	      servo->time = 0;
	    }
	    
	  }
	  servo->time -= SERVO_RATE_MS;
	  if (servo -> pc > 0) {
		servo-> pc--;

	  SetServoOutput(servo->pin, servo->pos, servo->count++);
	}
      }
	}
    }
  }
  return NULL;
  
}


Servo::Servo(int xpin)
{
  if(g_servoArray == NULL) {
    g_servoArray = new Array(16, 16, "Servos");
    pthread_create(&runServopth, NULL, runServo, NULL);
   }
  pin = xpin;
  pos = 0;
  time = 0;
  g_servoArray->addId(pin,(void *)this);
  count = 0;
  pc = 0;


}

Servo::Servo(char *sname, char *spin, char *spos)
{
  if(g_servoArray == NULL) {
    g_servoArray = new Array(16, 16, "Servos");
    pthread_create(&runServopth, NULL, runServo, NULL);
   }
  name = (string)sname;

  pin = atoi(spin);
  if(spos) {
    pos = atoi(spos);
  } else {
    pos = 0;
  }
  time = 0;
  g_servoArray->addId(pin,(void *)this);
  count = 0;
  pc = 0;
}

Servo::~Servo()
{
  g_servoArray->delId(pin, this);
}


void Servo::setVal(int val, int itime)
{
  rpos = val;
  time = itime;
  if(itime) {
    int steps = (itime / SERVO_RATE_MS) + 1 ;
    int dist = rpos - pos;
    if (dist < 0) dist = -dist;
    incr = dist / steps;
  } else {
    pos = val;
    time = 0;
  }
}

void Servo::setVal(char *spos, char *stime)
{
  int xtime = 0;
  int xpos = atoi(spos);
  if(stime)
    xtime = atoi(stime);
  setVal(xpos, xtime);
}

#ifdef _MAIN

int testServo(void *tdest, char *cmd, void *data)
{
  // servo name pin [pos]
  Servo *s;
  char scmd[128];
  char sname[128];
  char spin[128];
  char spos[128];
  int rc = sscanf(cmd, "%127s %127s %127s %127s", scmd, sname, spin, spos);
  if (rc < 3 ) {
    cout <<"Usage servo name pin [pos] " << endl;
    return 0;
  }
  if (rc > 3 ) {
    cout <<"servo name " << sname 
	 << " pin " << spin 
	 << " pos " << spos << endl;
    s = new Servo(sname, spin, spos);
  } else {
    cout <<"servo name " << sname 
	 << " pin " << spin 
	 << " pos " << "0" << endl;
    s = new Servo(sname, spin, (char *)"0");
  }
  return 0;
}

int testSetVal(void *tdest, char *cmd, void *data)
{
  // servo name val  [time]
  Servo *s;
  char scmd[128];
  char sname[128];
  char spos[128];
  char stime[128];
  int rc = sscanf(cmd, "%127s %127s %127s %127s", scmd, sname, spos, stime);
  if (rc < 3 ) {
    cout <<"Usage setVal name pos [time] " << endl;
    return 0;
  }
  if (rc > 3 ) {
    cout <<"servo name " << sname 
	 << " pos " << spos 
	 << " time " << stime << endl;
    setServoVal((string)sname, spos, stime);
  } else {
    cout <<"servo name " << sname 
	 << " pos" << spos 
	 << " time " << "0" << endl;
    setServoVal((string)sname, spos, (char *)"0");
  }
  return 0;
}



int testShowVal(void *tdest, char *cmd, void *data)
{
  // servo name val  [time]
  Servo *s;
  char scmd[128];
  char sname[128];
  char spos[128];
  char stime[128];
  int rc = sscanf(cmd, "%127s %127s %127s %127s", scmd, sname, spos, stime);
  if (rc < 2 ) {
    cout <<"Usage showVal name " << endl;
    return 0;
  }
  if (rc >= 2 ) {
    cout <<"servo name " << sname << endl;
    showServoVal((string)sname);
  } 
  return 0;
}

int main(int argc, char *argv[])
{

  Test *t = new Test("servo", "servo name pin [pos] create servo", testServo, NULL);
  t = new Test("setVal", "setval [pos,[time]]", testSetVal, NULL);
  t = new Test("showVal", "showval [name]", testShowVal, NULL);


  for (int i=1 ; i < argc ; i++ ) {
    runTest(NULL, argv[i]);
  }
  
  return 0;
}
#endif
