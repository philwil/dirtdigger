// test helper functions
//
// type > 16 is a data block ( min size 16)
// int,double or block
// strings are in blocks
// have a list and data groups


// as soon as a data item is declared we add access functions to the main socket
//
// actions before read and after set
// groups have actions as well.
#ifndef __TEST_H
#define __TEST_H


#include <iostream>
#include <string.h>
#include <string>
#include <sstream>


using namespace std;

int showTests(void *tdest, char *cmd, void *data);

int promptTests(void *tdest, char *cmd, void *data);

extern Array *g_testArray;


struct Test {

  Test(string tkey, string tdesc, int (*taction) (void *tdest, char *tcmd, void *data), void* tdata);

  ~Test(){
    if(g_testArray)g_testArray->_delItem(this);
  };

public:

  int match(char *tkey) {
    if (strcmp(tkey, key.c_str()) == 0 ) return 1;
    return 0;
  };

  string key;
  string desc;
  void * data;
  int (*action) (void *tdest, char *cmd, void *data);

};



int runTest(void *tdest, char *cmd);

#define showTest \
  do { \
    cout << endl << " ******* Running test <"	\
	 << __FUNCTION__\
	 << ">  ***********"\
	 << endl;\
} while (0);
#define endTest \
  do { \
    cout << endl << " ******* Endog test <"	\
	 << __FUNCTION__\
	 << ">  ***********"\
	 << endl;\
} while (0);

#endif
