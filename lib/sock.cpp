
// sock.cpp
// basic socket class
//
// g++ -o sock -D_MAIN sock.cpp -lpthread -lrt
// todo
// move work into sock.cpp from sock.h
// add command list
// add connect list
// The way it works is you simply add a command for a socket number 
// addCmd(5555, ,"show ",cmd_socks,NULL);
// addCmd(5556, ,"show ",cmd_socks,NULL);

#include <malloc.h>
#include <iostream>
#include <pthread.h>
#include <syslog.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <errno.h>
#include <cstdlib>
#include <unistd.h>

#include "sock.h"
#include "array.h"
#include "tests.h"

using namespace std;

int testServer(void *tdest, char *cmd, void *data);
int testClient(void *tdest, char *cmd, void *data);

static int g_local_debug = 0;
Array*g_srvArray= NULL;

SockCommand::SockCommand(string ckey, string chelp, int (*caction)(struct SockThread* st, char *msg, void *data), void *cdata)
{
  key = ckey;
  help = chelp;
  action = caction;
  data = cdata;
  return;  
};

// this is a client connection
Sock::Sock(string xaddr, int xport)
{
  int err;
  int i;

  type = SOCK_CLIENT;
  stArray=NULL;
  cmdArray=NULL;
  
  //char *ip="127.0.0.1";
  addr=xaddr;
  sockfd=socket(AF_INET,SOCK_STREAM,0);
  if(sockfd<0) {
    cout<<"Fail to create socket..."<<endl;
    return;
  }
  cout<<"Creating connection..."<<endl;
  serAdd.sin_family=AF_INET;
  serAdd.sin_addr.s_addr=inet_addr(addr.c_str());
  serAdd.sin_port=htons(port);
  if(connect(sockfd,(const sockaddr *)&serAdd,sizeof(serAdd))==0) {
    cout<<"Connected!"<<endl;
    cin>>i;      
  } else {
    cout<<"Connect failed..."<<endl;
    cin>>i;
  }
}  

// this is the main server
//we accept clients 
Sock::Sock(int xport)
{
  int enable = 1;
  
  type = SOCK_SERVER;
  stArray = NULL;
  cmdArray = NULL;
  
  //addr =  NULL;
  port = xport;
  sockfd=socket(AF_INET,SOCK_STREAM,0);
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
    cout <<"setsockopt(SO_REUSEADDR) failed"<<endl;
  if(sockfd<0) {
    cout<<"Fail to create socket..."<<endl;
    return;
  }
  cout<<"Binding..."<<endl;
  serAdd.sin_family=AF_INET;
  //serAdd.sin_addr.s_addr=inet_addr("127.0.0.1");
  serAdd.sin_addr.s_addr=INADDR_ANY;
  serAdd.sin_port=htons(port);
  if(bind(sockfd,(const sockaddr *)&serAdd,sizeof(serAdd))==0) {
    cout<<"Binding succeded!..."<<endl;
  } else {
    cout<<"Binding failed..."<<endl;
    // TODO close socket
    close(sockfd);
    sockfd = -1;
    return ;
  }
  if(listen(sockfd,1)==0) {
    cout<<"Listening on port " << port<< "..."<<endl;
  } else {
    cout<<"Listening failed..."<<endl;
    close(sockfd);
    sockfd = -1;
  }
  addCmd("help","show commands",cmd_help,NULL);
  addCmd("quit","Close connection",cmd_quit,NULL);
  
  return;

}

int Sock::saccept(void){
  socklen_t size = sizeof(cliAdd);
  int asock = accept(sockfd, (sockaddr *)&cliAdd, &size);
  if (asock>=0) {
    cout<<"Connection created!"<<endl;
  } else {
    cout<<"accept failed..."<<endl;
    //return -1;
  }
  return asock;
}

int Sock::addCmd(string key, string help, int (*action)(struct SockThread* st, char *msg, void *data) , void *data)
{
  if(cmdArray == NULL) {
    cmdArray = new Array(16,16,"Sock Cmds");
  }
  SockCommand *scmd = new SockCommand(key, help, action, data);
  cmdArray->addItem(scmd);
  return 0;
}
    

int Sock::runAccept(Array *srvArray)
{
  int fd;
  skdone = 0;
  srvArray->addItem((void*)this);
  if(stArray == NULL) {
    stArray = new Array(16,16, "Sock Threads");
  }
  while (!skdone) {
    fd = saccept();
    if(fd > 0) {
      struct SockThread *st = new SockThread(this, fd, NULL);
      stArray->addItem((void*)st);
    }
  }
}


int Sock::runCmd(SockThread *st, char *msg)
{
  Sock *sk = st->sk;
  int idx=0;
  char sp[1024];
  SockCommand *scmd = NULL;
  int rc;
  char cmd[128];
  rc = sscanf(msg, "%127s", cmd);
  if(rc > 0) {
    while (sk->cmdArray->findNext((void **)&scmd, idx,0)) {

      if(strcmp(cmd, scmd->key.c_str()) == 0) {
	rc = scmd->action(st, msg, scmd->data);
	cout << " Ran cmd " << scmd->key << " ] rc [" << rc<<"]" << endl;
	return rc;
      }
    }
  }
}

void *sockThread(void *data)
{
  int rc;
  SockThread *st = (SockThread *)data;
  Sock *sk = st->sk;
  char sp[1024];
  
  do {
    cout << " SockThread running fd " << st->sfd << endl;
    rc = read(st->sfd, st->buf, st->buf_len-1);
    if (rc > 0 ) st->buf[rc]=0;  
    cout << " read ["<<rc<<"] bytes data [" <<st->buf<<"]" << endl;
    if (rc > 0 ) {
      snprintf(sp, sizeof(sp), " Hello from port %d message # %d ->"
	       , sk->port
	       , st->msgnum
	       );
      st->msgnum++;
      sk->runCmd(st, st->buf);
      rc = write(st->sfd, sp, strlen(sp));
      rc = write(st->sfd, st->buf, rc);
    }
  } while (rc >0);
  close(st->sfd);
  return NULL;
}


void *sockAcceptThread(void *data)
{

  Sock *sk = (Sock*)data;
  do {
    cout << " SockAcceptThread running fd " << sk->sockfd << " port ["<< sk->port << endl;
    sk->runAccept(g_srvArray);
  } while (sk->sockfd > 0);
  return NULL;
}



// addCmd(5555, ,"show ",cmd_socks,NULL);
// int cmd_help(struct SockThread*st, char *msg, void *data)

int addCmd( int port, string key, string desc, int (*cmd)(struct SockThread*st, char *msg, void *data), void * data)
{
  int found = 0;
  if(g_srvArray == NULL)
    g_srvArray = new Array(16,16,"Srv Array");
  int idx = 0;
  Sock *sock=NULL;
  while(g_srvArray->findNext((void **)&sock, idx, 0)) {
    if(sock->port == port) {
      found = 1;
      break;
    }
  }
  //sock(5555)
  if(!found) {
    sock = new Sock(port);
    pthread_create(&sock->st, NULL, sockAcceptThread, (void *) sock);
    //pthread
    //sock->runAccept(g_srvArray);pthread
  }
  sock->addCmd(key.c_str(), desc.c_str(), cmd, data);
  // note that this must be in a thread
}

int cmd_help(struct SockThread*st, char *msg, void *data)
{
  Sock *sk = st->sk;
  int idx=0;
  char sp[1024];
  SockCommand *scmd = NULL;
  int rc;
  while (sk->cmdArray->findNext((void **)&scmd,idx,0)) {
    snprintf(sp, sizeof(sp), " Key [%s] Desc [%s]\n"
	    , scmd->key.c_str()
	    , scmd->help.c_str()
	    );
    rc = write(st->sfd, sp, strlen(sp));
  }
}

//g_srvArray);
int cmd_socks(struct SockThread*st, char *msg, void *data)
{
  Sock *sk = st->sk;
  int idx=0;
  char sp[1024];
  Sock *sock = NULL;
  int rc;
  while (g_srvArray->findNext((void **)&sock,idx,0)) {
    snprintf(sp, sizeof(sp), " Sock fd [%d] port [%d]\n"
	    , sock->sockfd
	    , sock->port
	    );
    rc = write(st->sfd, sp, strlen(sp));
  }
}

int cmd_quit(struct SockThread*st, char *msg, void *data)
{
  int rc;
  char *sp = (char *)" Goodbye\n";
  rc = write(st->sfd, sp, strlen(sp));
  close(st->sfd);
}

#include "sock.h"

#ifdef _MAIN




int testAddCmd(void *tdest, char *cmd, void *data)
{
  //int fd;
  int found = 0;
  int port = 5556;
  addCmd( port, "socks", "show server socks", cmd_socks, NULL);
  
  return 0;
}

//int test_client(void)

int testClient(void *tdest, char *cmd, void *data)
{
  Sock sk("127.0.0.1", 5555);
  return 0;
}

int testServer(void *tdest, char *cmd, void *data)
{
  //int fd;
  int found = 0;
  int port = 5555;
  if(g_srvArray == NULL)
    g_srvArray = new Array(16,16,"Serv Array");

  int idx = 0;
  Sock *sock=NULL;
  while(g_srvArray->findNext((void **)&sock, idx, 0)) {
    if(sock->port == port) {
      found = 1;
      break;
    }
  }
  //sock(5555)
  if(!found) {
    sock = new Sock(port);
    pthread_create(&sock->st, NULL, sockAcceptThread, (void *) sock);
    //pthread
    //sock->runAccept(g_srvArray);pthread
    cout<< " New server created for port "<< port  << endl;
  } else {
    cout<< " Server already created for port "<< port  << endl;
  }

  //Sock *sk = new Sock(5555);
  //pthread_create(&sk->st, NULL, sockAcceptThread, (void *) sk);
  //  sk->runAccept(g_srvArray);
  //g_srvArray->addItem(
  // TODO add to global list of sockets
  //fd = sk->saccept();
  //SockThread *st = new SockThread(sk, fd, NULL);
  //if(fd>0)close(fd);
  
  return 0;
}

int main(int argc, char *argv[])
{

  Test *t = new Test("server", "test Server", testServer, NULL);
  t = new Test("client", "test client", testClient, NULL);
  t = new Test("addCmd", "test addCmd", testAddCmd, NULL);

  for (int i=1 ; i < argc ; i++ ) {
    runTest(NULL, argv[i]);
  }
  
  return 0;
}


#endif
