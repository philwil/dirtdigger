// tests


#include <iostream>
#include <string.h>
#include <string>
#include <sstream>

#include "array.h"
#include "tests.h"

using namespace std;

Array *g_testArray = NULL;

Test::Test(string tkey, string tdesc, int (*taction) (void * tdest, char *tcmd, void *data), void* tdata) {
    key = tkey;
    desc = tdesc;
    action = taction;
    data = tdata;
    if(!g_testArray) {
      g_testArray = new Array(16,16,"Tests");
      Test *t = new Test("help", "Show tests", showTests, NULL);
      t = new Test("prompt", "Run prompt", promptTests, NULL);
    }
    g_testArray->addItem(this);

};

int  runTest(void *tdest, char *cmd)
{
  char key[128];
  sscanf(cmd, "%127s", key);
  int idx = 0;
  Test *test =  NULL;
  while( g_testArray->findNext((void **)&test, idx , 0)) {
    if (test->match(key)) {
      return test->action(tdest, cmd, test->data);
    } 
  }
  cout << " Test [" << key << "] not found" << endl;

}

int showTests(void *tdest, char *cmd, void *data)
{
   int idx=0;
  Test *test =  NULL;
  while( g_testArray->findNext((void **)&test, idx , 0)) {
    cout << " Test [" << test->key << "] desc ["<< test->desc<<" ]" << endl;
  }

  return 0;
}


int promptTests(void *tdest, char *cmd, void *data)
{

  int done = 0;
  string request;
  cout << " Entering prompt mode done or exit to quit " << endl;
  
  while (! done) {
    cout << " Enter a test (help OK)" << endl;
    getline (cin ,request);
    cout << " you entered  ["<< request <<"]" << endl;
    if (request == "done") done = 1;
    else if (request == "exit") done = 1;
    else if (request == "quit") done = 1;
    else {
      runTest(tdest, (char *)request.c_str());
    }
  }

  return 0;
}

